const jwt = require('jsonwebtoken');

exports.session= (req, res, next) => {
    if(req.session.role == "customer"){
        res.status(500)
        res.json({
            message:`you didnt have any permssion for this action`,
        })
    } else if (req.session.role == "admin"){
        next()
    }
}

exports.authenticationToken = (req, res, next) => {
    const authHeder = req.headers['authorization']
    const token =  authHeder && authHeder.split(' ')[1]
    if (token == null) {
        res.status(401) 
        return res.json({
            message:'Unauthorized'
        })
    }

    jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, (err, user) => {
        if (err) {
            res.status(403)
            return res.json({
                message:'forbidden, wrong token'
            })
        }  
        req.user = user
        next()
    })
   
}