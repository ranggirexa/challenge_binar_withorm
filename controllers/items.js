const {items} = require('../models')

exports.add_item = async (req, res) => {
    items.findAll({
        raw : true ,
        nest: true ,
        where:{
            item_name:req.body.item_name
        }
    })
    .then(resp => {
        if (resp.length == 0) {
            
            items.create({
                item_name:req.body.item_name,
                stock:req.body.stock,
                id_user:req.session.user_id
                
            })
            .then(resp => {
                res.json({
                    status:201,
                    message:'success insert data',
                    data:[
                       resp
                    ]
                })
            })
            .catch(err => {
                res.json({
                    status:403,
                    message:'failed insert data'
                })
            })
        }else{
            res.status(400)
            res.json({
                message:'failed, username was taken',
            })
        }
    })
    .catch(err => {
        res.status(500)
        res.json({
            message:err,
        })
    })
};


exports.show_item = async (req, res) => {
    items.findAll({
        raw : true ,
        nest: true 
    })
    .then(resp => {
        res.json({
            status:200,
            message:'success retrive data',
            data:[
               resp
            ]
        })
    })
    .catch(err => {
        res.status(500)
        res.json({
            message:err.message,
        })
    })
}

exports.show_detail_item = async (req, res) => {

    items.findOne({
        raw:true,
        nest:true,
        where:{
            id:req.params.id
        }
    }).then((resp) => {
        if (resp == null) {
            res.status(404)
            res.json({
                message:`data with id ${req.params.id} not found`,
                response:resp
            })
        }else{
            res.status(200)
            res.json({
                message:'success',
                response:resp
            })
        }
    }).catch((err) => {
        res.status(500)
        res.json({
            message:err.message
        })
    })
}

exports.update_item = async (req, res) => {
    items.update(
        {
            item_name :req.body.item_name,
                stock : req.body.stock,
                id_user : req.session.user_id
        },
        {
        where: {
            id: req.params.id
        },
        }
    )
        .then(function (resp) {
            res.status(201)
            res.json({
                message:'success'
            })
        })
        .catch(function (error) {
            console.log(error);
        });

}

exports.delete_item = async (req, res) => {
    items.destroy({
        where:{
            id:req.params.id
        }
    })
    .then(resp => {
        res.status(200)
        res.json({
            message:'success'
        })
    })
    .catch(err => {
        res.status(500)
        res.json({
            message:err.message
        })
    })

}