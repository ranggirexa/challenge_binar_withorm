const {items, Orders} = require('../models')

exports.add_order = async (req, res) => {
    items.findAll({
        raw : true ,
        nest: true ,
        where:{
            item_name:req.body.item_name
        }
    })
    .then(resp_item => {
        if (resp_item.length > 0) {
            Orders.create({
                id_customer:req.session.user_id,
                quantity:req.body.quantity,
                id_item:resp_item[0].id,
                status:"Success"
            })
            .then(resp_create_order => {
                items.update(
                    {
                        stock: resp_item[0].stock - req.body.quantity
                    },
                    {
                    where: {
                        item_name:req.body.item_name
                    },
                    }
                )
                .then(function (resp_update) {
                    // res.status(200)
                    // res.json({
                    //     message:'success'
                    // })
                    res.json({
                        status:201,
                        message:'success insert data',
                        data:[
                            resp_create_order
                        ]
                    })
                })
                .catch(function (error) {
                    res.status(500)
                    res.json({
                        message:` error update ${error}`,
                    })
                });
             
            })
            .catch(err => {
                res.json({
                    status:500,
                    message:err.message
                })
            })
        }else{
            res.status(500)
            res.json({
                message:'failed, item not found',
            })
        }
    })
    .catch(err => {
        res.status(500)
        res.json({
            message:err.message,
        })
    })

};

exports.show_order = async (req, res) => {
    Orders.findAll({
        raw : true ,
        nest: true 
    })
    .then(resp => {
        res.json({
            status:200,
            message:'success retrive data',
            data:[
               resp
            ]
        })
    })
    .catch(err => {
        res.status(500)
        res.json({
            message:err.message,
        })
    })
}

exports.show_detail_order = async (req, res) => {

    Orders.findOne({
        raw:true,
        nest:true,
        where:{
            id:req.params.id
        }
    }).then((resp) => {
        if (resp == null) {
            res.status(404)
            res.json({
                message:`data with id ${req.params.id} not found`,
                response:resp
            })
        }else{
            res.status(200)
            res.json({
                message:'success',
                response:resp
            })
        }
    }).catch((err) => {
        res.status(500)
        res.json({
            message:err.message
        })
    })
}

exports.update_order = async (req, res) => {
    Orders.update(
        {
            status:"canceled"
        },
        {
        where: {
            id: req.params.id
        },
        }
    )
        .then(function (resp) {
            res.status(200)
            res.json({
                message:'success'
            })
        })
        .catch(function (error) {
            console.log(error);
        });

}