# challenge_binar_withORM

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/ranggirexa/challenge_binar_withorm.git
git branch -M main
git push -uf origin main
```

<<<<<<< HEAD
## DOCUMENTATION

## auth

url : post 3300/register (register)
melakukan input data user dengan masukan username, password, role

url: post 3300/login (login)
melakukan login dengan skema melakukan pengecekan terhadap username lalu dilanjutkan melakukan pengecekan untuk password jika semua sesuai maka akan dilakukan generate access token untuk digunakan sebagai bukti authentikasi user telah melakukan login

url: delete 3300/logout (logout)
url ini untuk menghapus token pada refresh token (3300/refresh_token) sehingga jika token telah kadaluarsa user diharuskan login ulang

url : post 3300/refresh_token (refresh token)
untuk refresh access token agar bisa menlanjutkan session tanpa harus login ulang

## item

url:post 3300/item (add item)
url ini digunakan untuk melakukan penambhan item dengan ketentuan user yang melakukan aksi diharuskan login sebagai rol admin, jika tidak maka akan ditolak
dengan inputan item_name dan stock

url:get 3300/items (show all items)
url ini digunakan untuk menampilkan keseluruhan item

url:get 3300/items/:id (show detail item)
url ini digunakan untuk menampilkan item dengan id sesuai dengan parameter yang diinputkan pada url

url:put 3300/item/update/:id (update item)
url ini digunakan untuk memperbarui data item yang memiliki id yang sama dengan yang dimasukan kedalam parameter

url:delete 3300/item/delete/3
url ini digunakan untuk menghapus data item yang memiliki id yang sama dengan yang dimasukan kedalam parameter

## order
url:post 3300/order (add order)
url ini digunakan untuk melakukan penambhan order
dengan inputan item_name dan stock

url:get 3300/orders (show all orders)
url ini digunakan untuk menampilkan keseluruhan order

url:get 3300/orders/:id (show detail order)
url ini digunakan untuk menampilkan order dengan id sesuai dengan parameter yang diinputkan pada url

url:put 3300/item/update/:id (update order)
url ini digunakan untuk memperbarui status order yang memiliki id yang sama dengan yang dimasukan kedalam parameter

url:delete 3300/order/delete/3
url ini digunakan untuk menghapus data order yang memiliki id yang sama dengan yang dimasukan kedalam parameter
