require('dotenv').config()
const auths = require("../controllers/auth.js");
const items = require('../controllers/items.js')
const orders = require('../controllers/orders')
const middleware = require('../middleware/middleware')
const express = require('express');
const session = require('express-session')

const app = express();

app.use(session({
    resave: true,
    saveUninitialized: true,
    secret: "secret"
}))

app.listen(3300, ()=>{
    console.log("Sever is now listening at port 3300");
})

// client.connect();
app.use(express.json())


// app.get('/posts', middleware.authenticationToken,(req, res) =>{
//     console.log("aaa", req.session);
//     res.status(200)
//     res.json({
//         message:'success'
//     })
// })

app.get('/test', middleware.authenticationToken,(req, res) =>{
    console.log(req.session);
    res.status(200)
    res.json({
        message:'success'
    })
})


//auth

app.post("/logins",auths.login)

app.post("/refresh_token", auths.refresh_token);

app.delete("/logout", auths.logout);

app.post("/register", auths.register);

//items

app.post("/item" ,middleware.authenticationToken ,middleware.session, items.add_item)

app.get("/items", middleware.authenticationToken, items.show_item)

app.get("/item/:id", middleware.authenticationToken, items.show_detail_item)

app.put("/item/update/:id", middleware.authenticationToken ,middleware.session, items.update_item)

app.delete("/item/delete/:id", middleware.authenticationToken ,middleware.session, items.delete_item)


// //order

app.post("/order" ,middleware.authenticationToken, orders.add_order)

app.get("/orders" ,middleware.authenticationToken, orders.show_order)

app.get("/order/:id", middleware.authenticationToken, orders.show_detail_order)

app.put("/order/update/:id", middleware.authenticationToken ,middleware.session, orders.update_order)


